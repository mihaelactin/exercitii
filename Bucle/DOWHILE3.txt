#include <stdio.h>

int main()
{
    float nr, produs=1;
    printf("Introduceti un sir de numere reale:\n");
    scanf("%f", &nr);
    if(nr!=0)
    {
        do{
            produs=produs*nr;
            scanf("%f", &nr);
        }while(nr!=0);
        printf("Produsul numerelor este egal cu: %.2f", produs);
    }
    else
    {
        printf("Primul numar trebuie sa fie diferit de 0.");
    }
    return 0;
}
