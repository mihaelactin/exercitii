#include <stdio.h>

int main()
{
    int nr,nrCifre=0;
    printf("Introduceti un numar:\n");
    scanf("%d", &nr);
    if(nr<0)
    {
        nr=-nr;
    }
    do{
        nr=nr/10;
        nrCifre++;
    }while(nr>0);
    printf("Numarul introdus are %d cifre.", nrCifre);
    return 0;
}    
