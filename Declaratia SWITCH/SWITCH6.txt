
#include <stdio.h>

int main()
{
    char a;
    printf("Introduceti o litera:\n");
    scanf("%c", &a);
    switch(a)
    {
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
        case 'A':
        case 'E':
        case 'I':
        case 'O':
        case 'U':
            printf("Litera este o vocala.");
            break;
        default:
            printf("Litera este o consoana.");
    };
    return 0;
}