
#include <stdio.h>

int main()
{
    int n;
    printf("Introduceti un numar:\n");
    scanf("%d", &n);
    do
    {
        n=n/10;
    }
    while(n>=9);
    if(n==7)
    {
        printf("Prima cifra a numarului introdus este 7.");
    }
    else
    {
        printf("Prima cifra a numarului introdus NU este 7.");
    };
    return 0;
}
