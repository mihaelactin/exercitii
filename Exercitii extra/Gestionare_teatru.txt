#include <stdio.h>

struct nrLocuri         
{
    unsigned int nrRep;             //intrebare: este in regula sa existe liniile 5 si 16 concomitent?
    unsigned int nrBilete[25];
};

struct b
{
    struct nrLocuri nrLocuri;
};

struct incasari
{
    unsigned int nrRep;
    int suma[25];
};

int main()
{
    struct b B1[25];        //structura vector care stocheaza datele locurilor de catgorie 1
    struct b B2[25];        //structura vector care stocheaza datele locurilor de catgorie 2
    struct incasari SUMA1[25], SUMA2[25];       //structuri care stocheaza incasarile pentru fiecare piesa si fiecare categorie de loc
    int n;                  //nr spectacole
    unsigned int RP[25];    //nr reprezentatii
    int i, j, k, p1, p2;       //p1 si p2 - preturile locurilor de categ 1 si 2; k va fi folosit pentru a verifica numarul maxim de locuri din teatru
    int sumaTotala=0, sumaTotalaPiesa;      //var care stocheaza sumele totale pe fiecare piesa si suma totala pe teatru
    printf("Introduceti numarul de spectacole: ");
    scanf("%d", &n);
    if(n<=25)               //punem conditia ca n sa fie maxim 25 (atatea piese are teatrul)
    {
        printf("\nIntroduceti numarul de reprezentatii din fiecare spectacol:\n");
        for(i=0;i<n;i++)
        {
            scanf("%d", &RP[i]);
        }
        printf("\nIntroduceti pretul locurilor de categoria 1: ");
        scanf("%d", &p1);
        printf("\nIntroduceti pretul locurilor de categoria 2: ");
        scanf("%d", &p2);
        //verificam daca pretul categ 1 este mai mare decat pretul categ 2
        if(p1>p2)
        {
        //se introduc datele referitoare la numarul de bilete pentru fiecare spectacol si piesa
            for(i=0;i<n;i++)
            {
                B1[i].nrLocuri.nrRep=RP[i];  //se copie nr de reprezentatii
                B2[i].nrLocuri.nrRep=RP[i];
                printf("\nIntroduceti numarul de locuri pentru spectacolul %d", i+1);
                printf("\n\tCategoria 1 de locuri:");
                for(j=0;j<RP[i];j++)
                {
                    printf("\n\t\tSpectacol %d reprezentatia %d: ", i+1, j+1);
                    scanf("%d", &B1[i].nrLocuri.nrBilete[j]);
                    if(B1[i].nrLocuri.nrBilete[j]>364)      //categoria 1 de locuri are 13 randuri a 28 locuri => 364 locuri; daca se introduce o val mai mare de 364 se schimba val lui k
                    {
                        k++;
                    }
                }
                printf("\n\tCategoria 2 de locuri:");
                for(j=0;j<RP[i];j++)
                {
                    printf("\n\t\tSpectacol %d reprezentatia %d: ", i+1, j+1);
                    scanf("%d", &B2[i].nrLocuri.nrBilete[j]);
                     if(B2[i].nrLocuri.nrBilete[j]>336)
                    {
                        k++;                                //categ 2 de locuri are maxim 700-364 locuri => 336 locuri
                    }
                }
            }
            if(k==0)                        //daca k nu si-a schimbat val continui cu calcul si afisaz rezultate; daca k!=0 se va afisa un mesaj
            {
                for(i=0;i<n;i++)
                {
                    SUMA1[i].nrRep=RP[i];  //se copie nr de reprezentatii
                    SUMA2[i].nrRep=RP[i];
                    //calculam si stocam sumele incasate pentru fiecare piesa si categorie de locuri
                    for(j=0;j<RP[i];j++)
                    {
                        SUMA1[i].suma[j]=B1[i].nrLocuri.nrBilete[j]*p1;
                        SUMA2[i].suma[j]=B2[i].nrLocuri.nrBilete[j]*p2;
                    }
                }
                printf("\nIn stagiune au fost reprezentate %d piese de teatru", n);
                for(i=0;i<n;i++)
                {
                    printf("\n\tPiesa %d a avut %d reprezentatii", i+1, RP[i]);
                }
                //afisam sumele calculate si stocate pentru fiecare categorie de loc
                printf("\nSumele încasate pentru bilete de categoria 1:"); 
                for(i=0;i<n;i++)
                {
                    printf("\n\tPiesa %d: ", i+1);
                    for(j=0;j<RP[i];j++)
                    {
                        printf("%d ", SUMA1[i].suma[j]);
                    }
                }
                printf("\nSumele încasate pentru bilete de categoria 2:"); 
                for(i=0;i<n;i++)
                {
                    printf("\n\tPiesa %d: ", i+1);
                    for(j=0;j<RP[i];j++)
                    {
                        printf("%d ", SUMA2[i].suma[j]);
                    }
                }
                printf("\nIncasari totale pentru fiecare spectacol:");
                for(i=0;i<n;i++)
                {
                    printf("\n\tPiesa %d: ", i+1);
                    sumaTotalaPiesa=0;
                    for(j=0;j<RP[i];j++)
                    {
                        printf("%d ", SUMA1[i].suma[j]+SUMA2[i].suma[j]);       //adunam cele incasarile celor 2 categorii de locuri
                        sumaTotalaPiesa=sumaTotalaPiesa+SUMA1[i].suma[j]+SUMA2[i].suma[j];      //calculam incasarea totala pe piesa
                    }
                    printf("\tTOTAL %d", sumaTotalaPiesa);
                    sumaTotala=sumaTotala+sumaTotalaPiesa;   //calc incasarea totala pe teatru
                }
                printf("\nSuma totala incasata de teatru este: %d", sumaTotala);
            }
            else
            {
                printf("Numarul de locuri introduse depaseste capacitatea teatrului.");
            }
        }
        else
        {
            printf("\nPretul locurilor de categorie 1 trebuie sa fie mai mare");
        }
    }
    else
    {
        printf("\nNumarul de spectacole nu poate fi mai mare de 25.");
    }
    return 0;
}
