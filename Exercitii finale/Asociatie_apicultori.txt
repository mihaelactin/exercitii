
#include <stdio.h>

struct APICULTOR
{
    int s;              //nr stupi
    int tmApic[50];     //vectorul ce contine 0 si 1
    int recolta[50];    //recolta unui tip de miere
    int prodTotal;      //recolta totala
};

struct ASOCIATIE
{
    int n;              //nr membri
    int tm;             //nr tipuri de miere
    int codtm[50];      //cod tip miere
    struct APICULTOR apicultor[100];    //vector tip structura
};



int main()
{
    struct ASOCIATIE asoc;
    int prettm[50];         //pret tip de miere
    int i,j;
    int prodAsociatie=0;    //productie totala pe asociatie
    int incasTotalApicultor[50];
    int incasAsociatie=0;   //incasare totala asociatie
    int nrStupi[50], k=0;   //cu ajutorul vectorului nrStupi si a var k m-am asigurat ca nu se introduc mai multi stupi decat cei declarati pentru fiecare apicultor
    printf("Introduceti numarul de membri:");
    scanf("%d", &asoc.n);
    printf("\nIntroduceti numarul de tipuri de miere:");
    scanf("%d", &asoc.tm);
    printf("\nIntroduceti codurile tipurilor de miere si preturile acestora:");
    for(i=0;i<asoc.tm;i++)
    {
        printf("\ntm[%d] = ", i);
        scanf("%d", &asoc.codtm[i]);
        printf("prettm[%d] = ", i);
        scanf("%d", &prettm[i]);
    }    
    for(i=0;i<asoc.n;i++)
    {
        printf("\nIntroduceti numarul de stupi ai apicultorului[%d] = ", i);
        scanf("%d", &asoc.apicultor[i].s);
        asoc.apicultor[i].prodTotal=0;  //pentru fiecare apicultor in parte se vor initializa cu 0
        incasTotalApicultor[i]=0;
        nrStupi[i]=0;
        for(j=0;j<asoc.tm;j++)
        {
            printf("\nIntroduceti 1 sau 0 daca apicultorul[%d] are sau nu in recolta tipul de miere %d:\n", i, asoc.codtm[j]);
            scanf("%d", &asoc.apicultor[i].tmApic[j]);
            switch(asoc.apicultor[i].tmApic[j])
            {
                case 1:         //daca se introduce 1 se continua la introducerea recoltei
                {
                    printf("\nApicultorul[%d] a recoltat din tipul de miere %d: ", i, asoc.codtm[j]);
                    scanf("%d", &asoc.apicultor[i].recolta[j]);
                    nrStupi[i]++;       //calcul nr de stupi introdusi prin comanda anterioara
                }
                    break;
                case 0:         //daca s-a introdus 0 recolta va fi automat 0 pentru acel tip de miere
                {
                    asoc.apicultor[i].recolta[j]=0;
                }
                    break;
                default:
                {
                    printf("Invalid!");  //daca se introduce o alta valoare, programul se incheie
                    return 0;
                }
            }
            asoc.apicultor[i].prodTotal=asoc.apicultor[i].prodTotal+asoc.apicultor[i].recolta[j];
            incasTotalApicultor[i]=incasTotalApicultor[i]+asoc.apicultor[i].recolta[j]*prettm[j];
        }
        prodAsociatie=prodAsociatie+asoc.apicultor[i].prodTotal;
        incasAsociatie=incasAsociatie+incasTotalApicultor[i];
    }
    for(i=0;i<asoc.n;i++)       //verific daca pentru fiecare apicultor numarul de stupi declarat este egal cu nr de stupi introdus prin vectorul 0,1
    {                           //de asemenea se elimina si posibilitatea declararii mai multor stupi decat numarul de tipuri de miere existente
        if(nrStupi[i]==asoc.apicultor[i].s)
        {
            k++;
        }
    }
    if(k==asoc.n)
    {
        for(i=0;i<asoc.n;i++)
        {
            printf("\nProductia apicultorului[%d] este de %d kg", i, asoc.apicultor[i].prodTotal);
            printf("\nApicultorul[%d] incaseaza %d lei astfel:", i, incasTotalApicultor[i]);
            for(j=0;j<asoc.tm;j++)
            {
                printf("\n\tPentru mierea %d - %d lei", asoc.codtm[j], prettm[j]*asoc.apicultor[i].recolta[j]);
            }
        }
        printf("\nProductia totala a asociatiei este de %d kg", prodAsociatie);
        printf("\nIncasarile totale ale asociatiei sunt de %d lei", incasAsociatie);
    }
    else
    {
        printf("\nDatele au fost introduse eronat. Revizuiti!");
    }
    return 0;
}