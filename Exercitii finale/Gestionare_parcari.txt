#include <stdio.h>

struct PARCARI
{
    int id;
    int nrLoc;
    int pretOra;
};


int main()
{
    int n, p[3], i[50][50], h[50][50];
    struct PARCARI parcari[100];
    int j,k,l,m, nrParcari=0, hTotal=0;  //nrParcari contorizeaza numarul de parcari introduse de sectoare; hTotal reprezinta numarul total de ore de parcare
    int incasari[3], incasariTotale=0;   //vectorul incasari[3] memoreaza incasarile pentru fiecare sector in parte;
    float pretMediu, hMediu[50];        //pretMediu - pretul de parcare mediu; hMediu - numarul mediu de ore pentru fiecare parcare
    printf("Introduceti numarul de parcari:");
    scanf("%d", &n);
    //inregistram datele referitoare la parcari
    for(j=0;j<n;j++)
    {
        parcari[j].id=j;
        printf("Introduceti numarul de locuri pentru parcarea %d: ", j);
        scanf("%d", &parcari[j].nrLoc);
        printf("Introduceti pretul pe ora pentru parcarea %d: ", j);
        scanf("%d", &parcari[j].pretOra);
    }
    //inregistram datele introduse de fiecare sector in parte
    for(j=0;j<3;j++)
    {
        switch(j)
        {
            case 0:
            {
                printf("\nIntroduceti datele corespunzatoare sectorului verde:");
            }
                break;
            case 1:
            {
               printf("\nIntroduceti datele corespunzatoare sectorului alb:");
            }
                break;
            case 2:
            {
                printf("\nIntroduceti datele corespunzatoare sectorului albastru:");
            }
                break;
        }
        printf("\n\tNumar de parcari: ");
        scanf("%d", &p[j]);
        nrParcari=nrParcari+p[j];  //memoram numarul total de parcari introdus, apoi verificam sa nu depaseasca numarul n de parcari totale
        if(nrParcari<=n)
        {
            for(k=0;k<p[j];k++)
            {
                printf("\tIdentificator parcare: ");
                scanf("%d", &i[j][k]);
                if(i[j][k]>=0&&i[j][k]<n)   //ne asiguram ca identificatorul introdus este valid; poate lua valori de la 0 la n-1
                {
                    printf("\tNumar ore de parcare: ");
                    scanf("%d", &h[j][k]);
                }    
                else                        //daca nu este valid, se intrerupe programul
                {
                    printf("\nDate introduse invalide! Incercati din nou!");
                    return 0;
                }
            }
        }
        else                                //daca numarul de parcari introduse de sectoare depaseste nr n, se intrerupe programul
        {
            printf("\nDate introduse invalide! Incercati din nou!");
            return 0;
        }
    }
    //dupa introducerea datelor sectoarelor, verificam ca o parcare sa nu fie inregistrata in mai multe sectoare
    for(l=0;l<n;l++)
    {
        m=0;    //m inregistreaza numarul de aparitii al unei parcari in inregistrarile sectoarelor
        for(j=0;j<3;j++)
        {
            for(k=0;k<p[j];k++)
            {
                if(i[j][k]==l)
                {
                    m++; 
                }
            }
        }
        if(m>1)  //daca m > 1 inseamna ca o parcare a fost gasita in mai multe sectoare
        {
            printf("\nDate introduse invalide! O parcare poate apartine doar unui sector!\nIncercati din nou!");
            return 0;
        }
    }
    //incepem calculul datelor de iesire
    for(j=0;j<3;j++)
    {
        incasari[j]=0;   //incasare sector
        for(k=0;k<p[j];k++)
        {
           for(l=0;l<n;l++)   //pentru fiecare parcare a fiecarui sector verificam corespondenta cu parcarile din structura
           {
               if(i[j][k]==parcari[l].id)  //atunci cand se regaseste parcarea corespunzatoare, se calculeaza incasarea totala pentru fiecare sector in parte si timpul mediu de parcare
               {
                   incasari[j]=parcari[l].pretOra*h[j][k]+incasari[j];
                   hMediu[l]=(float)h[j][k]/parcari[l].nrLoc;           
               }
            }
           hTotal=hTotal+h[j][k];   //timpul total de parcare
        }
        //afisam incasarile pentru fiecare sector
        switch(j)
        {
            case 0:
            {
                printf("\nIncasari totale sector verde = %d", incasari[j]);
            }
                break;
            case 1:
            {
               printf("\nIncasari totale sector alb = %d", incasari[j]); 
            }
                break;
            case 2:
            {
                printf("\nIncasari totale sector albastru = %d", incasari[j]);
            }
                break;
        }
        incasariTotale=incasariTotale+incasari[j];   //calculam incasarile totale
    }
    pretMediu=(float)incasariTotale/hTotal;     //calculam pretul mediu
    printf("\nIncasari totale primarie: %d", incasariTotale);
    printf("\nNumarul total de ore de parcare: %d", hTotal);
    printf("\nPretul mediu pe ora: %.2f", pretMediu);
    //afisare timp mediu de parcare pentru fiecare parcare
    for(j=0;j<n;j++)
    {
        printf("\nNumarul mediu de ore de parcare pentru parcarea %d: %.2f", j, hMediu[j]);
    }
    return 0;
}