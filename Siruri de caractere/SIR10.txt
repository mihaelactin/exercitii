
#include <stdio.h>
#include <string.h>

int main()
{
   char sir[50], apa[4]="apa", fractie[4];
   int i, lungime, k=0;
   printf("Introduceti un cuvant:\n");
   scanf("%s", sir);
   lungime=strlen(sir);
   for(i=0;i<=lungime-2;i++) //sirul fractie va fi succesiunea de 3 litere din sirul sir
   {
        fractie[0]=sir[i];
        fractie[1]=sir[i+1];
        fractie[2]=sir[i+2];
        if(strcmp(fractie,apa)==0) //se verifica daca sirul fractie este acelasi cu sirul apa
        {
           k++; //k contorizeaza numarul de aparitii a sirului apa
        }
    }
    printf("\nCuvantul 'apa' se regaseste de %d ori.", k);
    return 0;
}    
