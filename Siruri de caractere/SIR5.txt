#include <stdio.h>

int main()
{
    char sir[50];
    int v[50]; //vectorul v va fi utilizat pentru a stoca numarul de aparitii a fiecarui caracter in parte din sir
    int i=0,j, k, nrAparitie, max, poz; //i va fi lungimea sirului, j si k vor parcurge sirul. max si poz ne ajuta sa aflam valoarea cea mai mare din vectorul vi si pozitia acesteia
    printf("Introduceti un sir de caractere:\n");
    scanf("%s", sir);
    while(sir[i]!='\0')
    {
        i++;
    }
    for(j=0;j<i;j++) //declaram vectorul v; v[j] va stoca nr de aparitii al caracterului sir[j]
    {
        nrAparitie=0;
        for(k=0;k<i;k++)
        {
            if(sir[j]==sir[k])
            {
                nrAparitie++;
            }
        }
        v[j]=nrAparitie;
    }
    //aflam care este valoarea cea mai mare din vectorul v si stocam pozitia acestuia pentru a putea returna ulterior caracterul sir[poz]
    max=v[0]; 
    poz=0;
    for(j=1;j<i;j++)
    {
        if(v[j]>max)
        {
            max=v[j];
            poz=j;
        }
    }
    printf("Caracterul '%c' apare de cele mai multe ori in sir.\nAcesta se regaseste de %d ori.", sir[poz], max);
    return 0;
}    
