#include <stdio.h>
#include <string.h>

int main()
{
    char cuvant[50], cuvantInvers[50];
    int lungime,i;
    printf("Introduceti un cuvant:\n");
    scanf("%s", cuvant);
    lungime=strlen(cuvant);
    for(i=0;i<lungime;i++)
    {
       cuvantInvers[lungime-i-1]=cuvant[i];
    }
    cuvantInvers[lungime]='\0';
    if(strcmp(cuvant, cuvantInvers)==0)
    {
        printf("\nCuvantul introdus este un palindrom.");
    }
    else
    {
        printf("\nCuvantul introdus NU este un palindrom.");
    }
    return 0;
}    
