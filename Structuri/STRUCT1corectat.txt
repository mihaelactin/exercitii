
#include <stdio.h>

struct CATALOG
{
    int nrMatricol;
    int varsta;
    float medie;
    char nume[50];
};

int main()
{
    struct CATALOG elev = {5879, 20, 8.25, "Constantin Mihaela"};
    printf("Numarul matricol: %d\n", elev.nrMatricol);
    printf("Varsta: %d\n", elev.varsta);
    printf("Medie: %.2f\n", elev.medie);
    printf("Nume: %s\n", elev.nume);
    return 0;
}