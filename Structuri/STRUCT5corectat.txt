#include <stdio.h>

struct DATA
{
    int zi;
    int luna;
    int an;
};

int main()
{
    struct DATA data1, data2;
    printf("Introduceti o data calendaristica:\nzi - \t");
    scanf("%d", &data1.zi);
    printf("luna - \t");
    scanf("%d", &data1.luna);
    printf("an - \t");
    scanf("%d", &data1.an);
    printf("\nIntroduceti a doua data calendaristica:\nzi - \t");
    scanf("%d", &data2.zi);
    printf("luna - \t");
    scanf("%d", &data2.luna);
    printf("an - \t");
    scanf("%d", &data2.an);
    if(data1.zi>=1&&data1.zi<=31&&data1.luna>=1&&data1.luna<=12&&data2.zi>=1&&data2.zi<=31&&data2.luna>=1&&data2.luna<=12)
    {
        if(data1.zi==data2.zi&&data1.luna==data2.luna&&data1.an==data2.an)
        {
            printf("\nDatele sunt egale.");
        }
        else
        {
            printf("\nDatele nu sunt egale.");
        }
    }    
    else
    {
        printf("\nDatele introduse nu sunt valide.");
    }
    return 0;
}