#include <stdio.h>

struct DATA
{
    int zi;
    int luna;
    int an;
};

int main()
{
    int nr_zile, p;
    struct DATA d_curenta, d_finala;
    printf("Introduceti data curenta:\nzi: ");
    scanf("%d", &d_curenta.zi);
    printf("luna: ");
    scanf("%d", &d_curenta.luna);
    printf("an: ");
    scanf("%d", &d_curenta.an);
    printf("Introduceti numarul de zile curs: ");
    scanf("%d", &nr_zile);
    d_finala.zi=d_curenta.zi+nr_zile;
    d_finala.luna=d_curenta.luna;
    d_finala.an=d_curenta.an;
    if(d_finala.luna==1||d_finala.luna==3||d_finala.luna==5||d_finala.luna==7||d_finala.luna==8||d_finala.luna==10||d_finala.luna==12)
        {
            p=31;
        
        }
        else if(d_finala.luna==4||d_finala.luna==6||d_finala.luna==9||d_finala.luna==11)
        {
            p=30;
        }
        else if(d_finala.luna==2)
        {
            if((d_finala.an%4==0&&d_finala.an%100!=0)||(d_finala.an%400==0))
            {
                p=29;
            }
            else
            {
                p=28;
            }
        }
    while(d_finala.zi>p)
    {
        if(d_finala.luna==1||d_finala.luna==3||d_finala.luna==5||d_finala.luna==7||d_finala.luna==8||d_finala.luna==10||d_finala.luna==12)
        {
            p=31;
        
        }
        else if(d_finala.luna==4||d_finala.luna==6||d_finala.luna==9||d_finala.luna==11)
        {
            p=30;
        }
        else if(d_finala.luna==2)
        {
            if((d_finala.an%4==0&&d_finala.an%100!=0)||(d_finala.an%400==0))
            {
                p=29;
            }
            else
            {
                p=28;
            }
        }
        d_finala.zi=d_finala.zi-p;
        d_finala.luna=d_finala.luna+1;
        while(d_finala.luna>12)
        {
            d_finala.luna=d_finala.luna-12;
            d_finala.an=d_finala.an+1;
        }
    }
    printf("Data finala este %d %d %d.", d_finala.zi, d_finala.luna, d_finala.an);
    return 0;
}
