#include <stdio.h>
int main()
{
    int parte_intreaga;
    float x, parte_fractionara, expresie;
    printf("Introduceti un numar real pozitiv x:\n");
    scanf("%f", &x);
    parte_intreaga=x;
    parte_fractionara=x-parte_intreaga;
    printf("[x] este egal cu: %d\n{x} este egal cu: %f", parte_intreaga, parte_fractionara);
    expresie=5*x*parte_intreaga-3*parte_fractionara+15;
    printf("\nExpresia calculata este egala cu: %f", expresie);
    return 0;
}