

#include <stdio.h>

int main()
{
    int v[20],v1[10],v2[10],i,j; //v este vectorul rezultat
    printf("Introduceti elementele vectorului v1:\n");
    for(i=1;i<=10;i++)
    {
        scanf("%d", &v1[i]);
    }
    printf("\nIntroduceti elementele vectorului v2:\n");
    for(i=1;i<=10;i++)
    {
        scanf("%d", &v2[i]);
    }
    for(i=2;i<=20;i=i+2) //pozitiile pare ale vectorului v
    {
        j=i/2;          //corespondenta dintre poz vectorului v si a vectorului v1(ex: v[2]=v1[1], v[4]=v1[2] etc)
        v[i]=v1[j];
    }
    for(i=1;i<=20;i=i+2) //pozitiile impare
    {
        j=(i+1)/2;      //v[1]=v2[1]; v[3]=v2[2] etc
        v[i]=v2[j];
    }
    printf("Vectorul final va fi:\n");
    for(i=1;i<=20;i++)
    {
        printf("%d ", v[i]);
    }
    return 0;
}
