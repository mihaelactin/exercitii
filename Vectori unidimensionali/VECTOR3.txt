
#include <stdio.h>

int main()
{
   int a[10], i, min, pozmin;
   printf("Introduceti elementele sirului:\n");
   for(i=0;i<10;i++)
   {
       printf("a[%d]=", i);
       scanf("%d", &a[i]);
   };
   min=a[0];
   for(i=1;i<10;i++)
   {
       if(a[i]<min)
       {
           min=a[i];
           pozmin=i;
       }
   };
   printf("Cel mai mic element este: a[%d]=%d", pozmin,min);
   return 0;
}
